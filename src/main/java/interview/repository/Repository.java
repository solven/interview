package main.java.interview.repository;

import main.java.interview.model.Empleado;
import main.java.interview.model.Empresa;
import main.java.interview.model.Sucursal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Repository {

    public List<Empleado> getEmpleados() {
        return Arrays.asList(
                new Empleado(1, "Juan", "Perez"),
                new Empleado(2, "Milagros", "Mendoza"),
                new Empleado(3, "Pablo", "Flores"),
                new Empleado(4, "George", "Lennon"),
                new Empleado(5, "Bob", "Orwell"),
                new Empleado(6, "Jimmy", "Vargas"),
                new Empleado(7, "Juan", "Loza"),
                new Empleado(8, "Domingo", "Castilla"),
                new Empleado(9, "Jose", "Ango"),
                new Empleado(10, "Maria", "Townsed"),
                new Empleado(11, "Fernanda", "Rodriguez")
        );
    }

    public List<Empresa> getEmpresas() {
        return Arrays.asList(
                new Empresa(1, "Solven Funding SAC"),
                new Empresa(2, "Banco Ripley"),
                new Empresa(3, "Fundacion Pepito's")
        );
    }

    public List<Sucursal> getSucursales() {
        return Arrays.asList(
                new Sucursal("Lima", 1, Arrays.asList(1, 3, 4)),
                new Sucursal("Trujillo", 1, Arrays.asList(2,5,6)),
                new Sucursal("Piura", 2, Arrays.asList(7,9)),
                new Sucursal("Lima", 2, Arrays.asList(8,10)),
                new Sucursal("Lima", 3, Arrays.asList(11))
        );
    }

}
